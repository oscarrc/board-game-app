var Observable = require('FuseJS/Observable');
var FileSystem = require("FuseJS/FileSystem");




var json;
var scenarioName = Observable('');
var characterList = Observable();
var difficultyName = Observable('');


function goBack(){
    json.selected_scenario = null;
    json.confirmed_scenario = null;
    json.selectedPlayersNum = null;
    json.selectedDifficulty = null;
    json.playersId = [];
    FileSystem.writeTextToFile(configsPath, JSON.stringify(json));

    router.push("MenuPage");
}



// Reads and map the observable inside scenarios.js & characters.js, containing the basic data
var scenarios = require("scenarios");
var characters = require("characters");



var configsPath = FileSystem.dataDirectory + "/" + "config.json";

// Triggers when page becomes active
function loadData() {
    FileSystem.readTextFromFile(configsPath)
    .then(function(result) {
        json = JSON.parse(result);

        scenarioName.value = scenarios.getAt(json.confirmed_scenario).name;
    
        var newCharList = [];
        for (var i = 0; i<json.playersId.length; i++) {
            newCharList.push(characters.where({id: json.playersId[i]}));
        }
        characterList.replaceAll(newCharList);

        difficultyName.value = scenarios.getAt(json.confirmed_scenario).difficulties[json.selectedDifficulty].name;
    });
}


function goToScenPrep() {
    router.goto("NGScenPrep");
}


module.exports = {
    goBack: goBack,
    loadData: loadData,
    scenarioName: scenarioName,
    characterList: characterList,
    difficultyName: difficultyName,
    goToScenPrep: goToScenPrep
};
