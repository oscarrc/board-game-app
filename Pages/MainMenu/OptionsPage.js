var FileSystem = require("FuseJS/FileSystem");
var Observable = require("FuseJS/Observable");


function goBack() {
    router.goBack();
}

var configsPath = FileSystem.dataDirectory + "/" + "config.json";

var configs = {};
var ambMus = new Observable();
var anims = new Observable();
var extras = new Observable();

FileSystem.readTextFromFile(configsPath)
.then(function(result) {
    configs = JSON.parse(result);
    ambMus.value = configs.ambMus;
    anims.value = configs.anims;
    extras.value = configs.extras;
});


function changeAmbMus() {
    configs.ambMus = ambMus.value;
    FileSystem.writeTextToFile(configsPath, JSON.stringify(configs));
}

function changeAnims() {
    configs.anims = anims.value;
    FileSystem.writeTextToFile(configsPath, JSON.stringify(configs));
}

function changeExtras() {
    configs.extras = extras.value;
    FileSystem.writeTextToFile(configsPath, JSON.stringify(configs));
}

module.exports = {
    goBack: goBack,
    ambMus: ambMus,
    anims: anims,
    extras: extras,
    changeAmbMus: changeAmbMus,
    changeAnims: changeAnims,
    changeExtras: changeExtras
};