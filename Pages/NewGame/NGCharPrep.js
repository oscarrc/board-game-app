var Observable = require('FuseJS/Observable');
var FileSystem = require("FuseJS/FileSystem");
var characters = require("characters");




var preparationText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n\nUt egestas odio nibh, nec ornare sem suscipit vel. In dictum tellus ut dolor placerat eleifend. Nunc bibendum ac lorem at posuere. Ut sagittis augue justo. Sed ultricies venenatis nibh ut euismod. \n\nUt accumsan urna nunc, eget ullamcorper nunc scelerisque blandit. Etiam scelerisque purus vitae congue vestibulum. Aenean sit amet tempor mauris. Quisque ut quam mollis, volutpat ligula non, hendrerit tellus. Cras pretium ut tortor a malesuada.";

var characterList = Observable();
var activePageIndex = Observable();
var leftArrowVisible = Observable('Collapsed');
var rightArrowVisible = Observable('Collapsed');


var json;

function loadData() {
    var configsPath = FileSystem.dataDirectory + "/" + "config.json";
    FileSystem.readTextFromFile(configsPath)
        .then(function(result) {
            json = JSON.parse(result);

            var newCharList = [];
            for (var i = 0; i<json.playersId.length; i++) {
                newCharList.push(characters.where({id: json.playersId[i]}));
            }
            characterList.replaceAll(newCharList);
            
        });
}


function goToMenu() {
    router.goto("MenuPage");
}





// Triggered every time the activePageIndex observable change
activePageIndex.onValueChanged(module, function() {
    if (json) {
        if (activePageIndex.value === 0) {
            leftArrowVisible.value = 'Collapsed';
        } else {
            leftArrowVisible.value = 'Visible';
        }

        if (activePageIndex.value == (json.playersId.length - 1)) {
            rightArrowVisible.value = 'Collapsed';
        } else {
            rightArrowVisible.value = 'Visible';
        }
}
});


function priorCharacter() {
    activePageIndex.value --;
}

function nextCharacter() {
    activePageIndex.value ++;
}


module.exports = {
    preparationText: preparationText,
    activePageIndex: activePageIndex,
    leftArrowVisible: leftArrowVisible,
    rightArrowVisible: rightArrowVisible,
    characterList: characterList,
    loadData: loadData,
    goToMenu: goToMenu,
    priorCharacter: priorCharacter,
    nextCharacter: nextCharacter
};