var Observable = require("FuseJS/Observable");
var FileSystem = require("FuseJS/FileSystem");



function Player(id, image, name){
    this.id = id;
    this.image = Observable(image);
    this.name = Observable(name);
    this.isCharacterSelected = Observable(false);
    this.selectedCharacter = '';
}

var playersNum = Observable(0);
var minPlayers = Observable(0);
var maxPlayers = Observable(0);
var playerList = Observable();
var playerBeingSelected;
var allPlayerSelected = Observable(false);



// Reads and map the observable inside scenarios.json, containing the basic data
var scenarios = require("scenarios");




var json;
var configsPath = FileSystem.dataDirectory + "/" + "config.json";

// Called when the page becomes Active.
function refreshData(e){
	FileSystem.readTextFromFile(configsPath)
    	.then(function(result) {
			json = JSON.parse(result);
            if(json.confirmed_scenario != 'none'){
                playersNum.value = scenarios.toArray()[json.confirmed_scenario].minPlayers;
                minPlayers.value = scenarios.toArray()[json.confirmed_scenario].minPlayers;
                maxPlayers.value = scenarios.toArray()[json.confirmed_scenario].maxPlayers;
            }
        });
}

// Called whenever the playersNum observable changes its value
function fillPlayersReticula() {

    resetCharacters();

    var newPlayerList = [];
    for(var i = 0; i < playersNum.value; i++) {
        newPlayerList.push(new Player(i, '', ''));
    }
    playerList.replaceAll(newPlayerList);
}

function playerSelect(x){
    playerBeingSelected = x.data.id;
}









/////////////////////// Character selector js ////////////////////
var characters = require("characters"); // Reads and map the observable inside characters.json

var resetting = Observable(false);
var visibleCharacter = 0;

function selected(x) {
    // If the player had already selected a char, return that char to the characters Observable
    if (playerList.getAt(playerBeingSelected).isCharacterSelected.value === true) {
        characters.add(playerList.getAt(playerBeingSelected).selectedCharacter);
    }

    // Change the visual info
    playerList.getAt(playerBeingSelected).image.value = characters.getAt(visibleCharacter).image;
    playerList.getAt(playerBeingSelected).name.value = characters.getAt(visibleCharacter).name;
    playerList.getAt(playerBeingSelected).isCharacterSelected.value = true;

    // Change the char Observable so the char won't be abailable anymore
    playerList.getAt(playerBeingSelected).selectedCharacter = characters.getAt(visibleCharacter);
    characters.remove(characters.getAt(visibleCharacter));

    // The navigation restarts, and so the variable that tells the visible character
    visibleCharacter = 0;
    
    // Writes that character in the playersId, inside config.json
	var configsPath = FileSystem.dataDirectory + "/" + "config.json";
	FileSystem.readTextFromFile(configsPath)
    .then(function(result) {
		json = JSON.parse(result);
        json.playersId.push(x.data.id);
		FileSystem.writeTextToFile(configsPath, JSON.stringify(json));
    });

    // If all players has selected characters, go to next page (Difficulty)
    var allPlayersReady = true;
    for (var i = 0; i < playerList.length; i++) {
        if (playerList.getAt(i).isCharacterSelected.value === false) {
            allPlayersReady = false;
        }
    }
    allPlayerSelected.value = allPlayersReady
}

function notSelected(x) {
    visibleCharacter ++;
}

function reset(x) {
    visibleCharacter = 0;
    resetting.value = true;
    setTimeout(backToNormal, 300);
}

function backToNormal() {
    resetting.value = false;
}

function resetCharacters() {
    for (var i = 0; i < playerList.length; i++) {
        characters.add(playerList.getAt(i).selectedCharacter);
    }
    charactersDeleted = [];
}

reset();








        
        
module.exports = {
    playersNum: playersNum,
    minPlayers: minPlayers,
    maxPlayers: maxPlayers,
    refreshData: refreshData,
    fillPlayersReticula: fillPlayersReticula,
    playerList: playerList,
    playerSelect: playerSelect,
    allPlayerSelected: allPlayerSelected,


    characters: characters.map(function(item, index){
        item.degrees = -4 + (8 * Math.random());
        return item;
    }),
    resetting: resetting,
    selected: selected,
    notSelected: notSelected,
    reset: reset,
    resetCharacters: resetCharacters
};