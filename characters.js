var Observable = require("FuseJS/Observable");

module.exports = Observable(
	{
		id: 0,
		name: 'Knight',
		description: 'An obscure knight of poor renown who collapsed roaming the land. Sturdy, owing to high vitality and stout armor.',
		tip1: 'Recommended class for new players.',
		tip2: 'Intelligence and Faith stats are average enough to use spells soon.',
		image: "Assets/Images/character01_card.png",
		faceImage: "Assets/Images/character01_face.png",
		strength: 14,
		dexterity: 12,
		wisdom: 7,
		faith: 8,
		isAvailable: true,
		prepCards: [
			"Assets/Images/item_card01.png",
			"Assets/Images/item_card02.png",
			"Assets/Images/item_card03.png",
			"Assets/Images/item_card04.png"
		]
	},
	{
		id: 1,
		name: 'Warrior',
		description: 'Descendant of northern warriors famed for their brawn. Utilizes high strength to wield a heavy battleaxe.',
		tip1: 'Ideal choice for pure melee builds.',
		tip2: 'Starts with highest Strength stat.',
		image: "Assets/Images/character02_card.png",
		faceImage: "Assets/Images/character02_face.png",
		strength: 15,
		dexterity: 12,
		wisdom: 8,
		faith: 8,
		isAvailable: true,
		prepCards: [
			"Assets/Images/item_card04.png",
			"Assets/Images/item_card03.png",
			"Assets/Images/item_card01.png"
		]
	},
	{
		id: 2,
		name: 'Herald',
		description: 'A former herald who journeyed to finish a quest undertaken. Wields a sturdy spear and employs a gentle restorative miracle.',
		tip1: 'Has better starting equipment, but less powerful miracle than the cleric.',
		tip2: 'His starting miracle makes it worth to support other players from begining.',
		image: "Assets/Images/character03_card.png",
		faceImage: "Assets/Images/character03_face.png",
		strength: 13,
		dexterity: 13,
		wisdom: 7,
		faith: 10,
		isAvailable: true,
		prepCards: [
			"Assets/Images/item_card02.png",
			"Assets/Images/item_card04.png",
			"Assets/Images/item_card03.png",
			"Assets/Images/item_card01.png"
		]
	},
	{
		id: 3,
		name: 'Thief',
		description: 'A common thief and pitiful deserter. Wields a dagger intended for backstabs alongside a military-issue bow.',
		tip1: 'Starts with highest Dexterity stat.',
		tip2: 'The only class that starts with a bow.',
		image: "Assets/Images/character04_card.png",
		faceImage: "Assets/Images/character04_face.png",
		strength: 10,
		dexterity: 15,
		wisdom: 7,
		faith: 8,
		isAvailable: true,
		prepCards: [
			"Assets/Images/item_card03.png",
			"Assets/Images/item_card02.png"
		]
	},
	{
		id: 4,
		name: 'Assassin',
		description: 'An assassin who stalks their prey from the shadows. Favors sorceries in addition to thrusting swords.',
		tip1: 'Spook could be considered one of the most useful starting spells.',
		tip2: 'Good to start with if you want to use sorceries as well as normal combat effectively.',
		image: "Assets/Images/character05_card.png",
		faceImage: "Assets/Images/character05_face.png",
		strength: 12,
		dexterity: 13,
		wisdom: 9,
		faith: 10,
		isAvailable: true,
		prepCards: [
			"Assets/Images/item_card04.png",
			"Assets/Images/item_card03.png",
			"Assets/Images/item_card01.png"
		]
	},
	{
		id: 5,
		name: 'Sorcerer',
		description: 'A loner who left formal academia to pursue further research. Commands soul sorceries using high intelligence.',
		tip1: 'Starts with Heavy Soul Arrow, handy for sniping down enemies.',
		tip2: 'Recommended class for average players.',
		image: "Assets/Images/character06_card.png",
		faceImage: "Assets/Images/character06_face.png",
		strength: 11,
		dexterity: 12,
		wisdom: 8,
		faith: 14,
		isAvailable: true,
		prepCards: [
			"Assets/Images/item_card01.png",
			"Assets/Images/item_card04.png",
			"Assets/Images/item_card03.png",
			"Assets/Images/item_card02.png"
		]
	},
	{
		id: 6,
		name: 'Pyromancer',
		description: 'A pyromancer from a remote region who manipulates flame. Also an adept close combat warrior who wields a hand axe.',
		tip1: 'Starts with average Strength and Dexterity.',
		tip2: 'Starts with pyromance spells that other classes find late on the game.',
		image: "Assets/Images/character07_card.png",
		faceImage: "Assets/Images/character07_face.png",
		strength: 13,
		dexterity: 14,
		wisdom: 7,
		faith: 8,
		isAvailable: true,
		prepCards: [
			"Assets/Images/item_card03.png"
		]
	},
	{
		id: 7,
		name: 'Cleric',
		description: 'A traveling cleric who collapsed from exhaustion. Channels high faith to cast many and varied miracles.',
		tip1: 'Has the highest Faith of all the classes and has a good Strength.',
		tip2: 'His miracles makes him have another healing source other than Estus Flasks.',
		image: "Assets/Images/character08_card.png",
		faceImage: "Assets/Images/character08_face.png",
		strength: 13,
		dexterity: 11,
		wisdom: 9,
		faith: 13,
		isAvailable: true,
		prepCards: [
			"Assets/Images/item_card01.png",
			"Assets/Images/item_card04.png"
		]
	}
);
 
