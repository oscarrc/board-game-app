var Observable = require('FuseJS/Observable');
var FileSystem = require("FuseJS/FileSystem");

var activePage = Observable(0);

var scenarioSelected = Observable(false);
var playersReady = Observable(false);
var difficultySelected = Observable(false);

var backButtonVisibility = Observable('Visible');


function gotoNextPage() {
    activePage.value ++;
}

function gotoPrevPage() {
    activePage.value --;
}




function goBack(){
    scenarioSelected.value =  false;
    playersReady.value =  false;
    difficultySelected.value =  false;

    var configsPath = FileSystem.dataDirectory + "/" + "config.json";
    FileSystem.readTextFromFile(configsPath)
    .then(function(result) {
        var json = JSON.parse(result);
        json.selected_scenario = null;
        json.confirmed_scenario = null;
        json.selectedPlayersNum = null;
        json.selectedDifficulty = null;
        json.playersId = [];

        FileSystem.writeTextToFile(configsPath, JSON.stringify(json));
    });

    router.goBack();
}

function goToConfirmationPage() {
    scenarioSelected.value =  false;
    playersReady.value =  false;
    difficultySelected.value =  false;
    
	router.push("NGConfirmationPage");
}



// Reads the observable inside characters.json, containing the basic data
var characters = require("characters");
var characterList = characters.toArray();


function City(name, imageKey, country, visitors, nonVisitors){
	this.name = name;
	this.imageKey = imageKey;
	this.country = country;
	this.visitors = Observable(visitors);
	this.nonVisitors = Observable(nonVisitors);
	this.degrees = -4 + (8 * Math.random());
}

var objectList = Observable();
for (var i = 0; i < characterList.length; i++) {
    objectList.add(new City("Oslo", "Assets/Images/character01_card.png", "NORWAY", 3127, 3943));
}
var stringObjectList = JSON.stringify(objectList.toArray());




function setScenarioSelected() {
    scenarioSelected.value =  true;
}

function setPlayersReady() {
    playersReady.value =  true;
}

function unsetPlayersReady() {
    playersReady.value =  false;
}

function setDifficultySelected() {
    difficultySelected.value =  true;
}



function toggleBackButtonVisibility() {
    if (backButtonVisibility.value === 'Collapsed') {
        backButtonVisibility.value = 'Visible';
    } else {
        backButtonVisibility.value = 'Collapsed';
    }
}





module.exports = {
    goBack: goBack,
    goToConfirmationPage: goToConfirmationPage,
    activePage: activePage,
    scenarioSelected: scenarioSelected,
    playersReady: playersReady,
    difficultySelected: difficultySelected,
    setScenarioSelected: setScenarioSelected,
    setPlayersReady: setPlayersReady,
    setDifficultySelected: setDifficultySelected,
    unsetPlayersReady: unsetPlayersReady,
    gotoNextPage: gotoNextPage,
    gotoPrevPage: gotoPrevPage,
    stringObjectList: stringObjectList,
    backButtonVisibility: backButtonVisibility,
    toggleBackButtonVisibility: toggleBackButtonVisibility,
};