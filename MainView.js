var FileSystem = require("FuseJS/FileSystem");
var Observable = require('FuseJS/Observable');
var Bundle = require("FuseJS/Bundle");

var scenVideoFile = Observable();

function refreshInfo() {
  setTimeout(function() {
    
// Process user config file
var configsPath = FileSystem.dataDirectory + "/" + "config.json";
FileSystem.readTextFromFile(configsPath)
    .then(function(result) {
        console.log('Readed existing config.');
        chooseScenarioVideo(JSON.parse(result).selected_scenario);
    })
    .catch(function(error) {
        console.log("Unable to read file due to error:" + error);
        console.log('Writting default config file.');
        
        // Read the local configurations json
        Bundle.read("config.json")
	      .then(function(result) {
            // Write these configurations into a new device local file
            FileSystem.writeTextToFile(configsPath, result);
            chooseScenarioVideo("default");
        });
    });

    }, 100);
  }

  refreshInfo();


// Change the background video file
function chooseScenarioVideo(scenario) {
    switch(scenario) {
        case "default":
          scenVideoFile.value = 'Assets/Videos/backgroundDefault.mp4';
          break;
        case 0:
          scenVideoFile.value = 'Assets/Videos/background01.mp4';
          break;
        case 1:
          scenVideoFile.value = 'Assets/Videos/background02.mp4';
          break;
        case 2:
          scenVideoFile.value = 'Assets/Videos/background03.mp4';
          break;
        case 3:
          scenVideoFile.value = 'Assets/Videos/background04.mp4';
          break;
        case 4:
          scenVideoFile.value = 'Assets/Videos/background05.mp4';
          break;
      }
}

module.exports = {
    scenVideoFile: scenVideoFile,
    refreshInfo: refreshInfo
};