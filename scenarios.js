var Observable = require("FuseJS/Observable");

module.exports = Observable(
	{
		id: 0,
		name: 'The Sinking City',
		type: 'Base',
		color: '#000000',
		image: "Assets/Images/scenario01_background.png",
		video: "../Assets/Videos/background01.mp4",
		minPlayers: 1,
		maxPlayers: 5,
		time: "1,30h±",
		facts: [
			"Sharn is the most populous city in all of Khorvaire, and arguably all of Eberron. The city literally towers atop a cliff above the mouth of the Dagger River in southern Breland. Sharn is known as The Sinking City, but has also been called many other names, including The City of Knives, The City of Lost Souls, The City of a Thousand Eyes, the Gateway to Ky'lohg, and The Gateway to Perdition. Here comes a really large description that has to have extra lines to test the scroll vire in the scenario description pages, so here we go.",
			'The Great Basin Bristlecone Pine (Pinus longaeva) trees grow between 9,800 and 11,000 feet (3,000–3,400 m) above sea level',
			"For many years, it was the world's oldest known living non-clonal organism."
		],
		isConfirmed: Observable(false),
		difficultyBackgroundImage: 'Assets/Images/difficulty_background01.jpg',
		difficulties: [
			{
				id: 0,
				name: 'Easy',
				difficultyInfo: "Stronger enemies. Play time will also increase."
			},
			{
				id: 1,
				name: 'Medium',
				difficultyInfo: "Stronger and high healthy enemies. Play time will also increase."
			},
			{
				id: 2,
				name: 'Hard',
				difficultyInfo: "Stronger and high healthy enemies. Play time will also increase, as well the quality of random objects."
			},
			{
				id: 3,
				name: 'Extreme',
				difficultyInfo: "Stronger and heavy healthy enemies. Play time will also highly increase, as well the quality of random objects."
			}
		],
		scenPrep: {
			tokens: [
				{
					token_image: 'Assets/Images/token01.png',
					token_size: 60,
					token_amount: 15
				},
				{
					token_image: 'Assets/Images/token02.png',
					token_size: 60,
					token_amount: 20
				},
				{
					token_image: 'Assets/Images/token03.png',
					token_size: 60,
					token_amount: 30
				},
				{
					token_image: 'Assets/Images/token04.png',
					token_size: 110,
					token_amount: 10
				},
				{
					token_image: 'Assets/Images/token04.png',
					token_size: 110,
					token_amount: 7
				}
			],
			considerations:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n\nUt egestas odio nibh, nec ornare sem suscipit vel. In dictum tellus ut dolor placerat eleifend. Nunc bibendum ac lorem at posuere. Ut sagittis augue justo. Sed ultricies venenatis nibh ut euismod. \n\nUt accumsan urna nunc, eget ullamcorper nunc scelerisque blandit. Etiam scelerisque purus vitae congue vestibulum. Aenean sit amet tempor mauris. Quisque ut quam mollis, volutpat ligula non, hendrerit tellus. Cras pretium ut tortor a malesuada. Morbi varius molestie eleifend. Mauris mattis, tortor id suscipit condimentum, lacus justo viverra leo, tempor faucibus quam eros id arcu. Pellentesque eros ligula, malesuada id erat non, semper posuere odio. Cras at faucibus nibh, sit amet dictum elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin elementum nulla sit amet neque hendrerit, eu lobortis dolor tempus. Aenean congue luctus mattis. Pellentesque in aliquam eros. Duis ac sem condimentum, consequat nibh maximus, pretium lorem. Vivamus dignissim ipsum quis odio vulputate iaculis. Praesent ultrices posuere dolor, a lacinia lacus facilisis id. Mauris tristique libero urna, sed fermentum arcu auctor id. In blandit risus orci, nec pharetra sem convallis pretium. Ut sed suscipit ipsum. Donec vulputate et sapien nec vehicula. Duis bibendum nec nisl at semper. Quisque sodales faucibus justo vel laoreet. Aenean consectetur ex ac urna ornare, eget ultricies lectus iaculis. Curabitur congue congue erat vitae malesuada. Pellentesque varius nunc nibh, ut tincidunt elit iaculis a. Curabitur dictum eget arcu sed faucibus. In vitae turpis ac odio dapibus facilisis. \n\nAliquam et mauris sed neque mattis placerat eu id nisl. Fusce quis odio in libero posuere pharetra vel at nunc. Integer eu pharetra tellus, at aliquam velit. Duis laoreet tortor vel efficitur gravida. Pellentesque a ex quis risus iaculis luctus. Nam at diam ut odio tempor lobortis at a erat. In vitae felis eget augue rhoncus tempor elementum quis nisl. Curabitur malesuada justo sagittis arcu interdum egestas.'
		}
	},
	{
		id: 1,
		name: 'The Black Abyss',
		type: 'Base',
		color: '#000000',
		image: "Assets/Images/scenario02_background.png",
		video: "../Assets/Videos/background02.mp4",
		minPlayers: 1,
		maxPlayers: 3,
		time: "1h±",
		facts: [
			"An Abyss seems to be the result of rampant, uncontrolled humanity expanding and swallowing up people and places. The first Abyss in recorded history was generated when Iraeth' humanity went wild from his madness, corrupting life and leaving behind an infinite chasm of darkness with it.",
			'According to Landnámabók, the settlement of Iceland began in the year 874 AD when the Norwegian chieftain Ingólfr Arnarson became the first permanent settler on the island.',
			'Iceland has a market economy with relatively low taxes compared to other OECD countries.'
		],
		isConfirmed: Observable(false),
		difficultyBackgroundImage: 'Assets/Images/difficulty_background02.jpg',		difficulties: [
			{
				id: 0,
				name: 'Easy',
				difficultyInfo: "Stronger enemies. Play time will also increase."
			},
			{
				id: 1,
				name: 'Medium',
				difficultyInfo: "Stronger and high healthy enemies. Play time will also increase."
			},
			{
				id: 2,
				name: 'Hard',
				difficultyInfo: "Stronger and high healthy enemies. Play time will also increase, as well the quality of random objects."
			}
		],
		scenPrep: {
			tokens: [
				{
					token_image: 'Assets/Images/token01.png',
					token_size: 60,
					token_amount: 15
				},
				{
					token_image: 'Assets/Images/token02.png',
					token_size: 60,
					token_amount: 20
				},
				{
					token_image: 'Assets/Images/token03.png',
					token_size: 60,
					token_amount: 30
				},
				{
					token_image: 'Assets/Images/token04.png',
					token_size: 110,
					token_amount: 10
				},
				{
					token_image: 'Assets/Images/token04.png',
					token_size: 110,
					token_amount: 7
				}
			],
			considerations:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n\nUt egestas odio nibh, nec ornare sem suscipit vel. In dictum tellus ut dolor placerat eleifend. Nunc bibendum ac lorem at posuere. Ut sagittis augue justo. Sed ultricies venenatis nibh ut euismod. \n\nUt accumsan urna nunc, eget ullamcorper nunc scelerisque blandit. Etiam scelerisque purus vitae congue vestibulum. Aenean sit amet tempor mauris. Quisque ut quam mollis, volutpat ligula non, hendrerit tellus. Cras pretium ut tortor a malesuada. Morbi varius molestie eleifend. Mauris mattis, tortor id suscipit condimentum, lacus justo viverra leo, tempor faucibus quam eros id arcu. Pellentesque eros ligula, malesuada id erat non, semper posuere odio. Cras at faucibus nibh, sit amet dictum elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin elementum nulla sit amet neque hendrerit, eu lobortis dolor tempus. Aenean congue luctus mattis. Pellentesque in aliquam eros. Duis ac sem condimentum, consequat nibh maximus, pretium lorem. Vivamus dignissim ipsum quis odio vulputate iaculis. Praesent ultrices posuere dolor, a lacinia lacus facilisis id. Mauris tristique libero urna, sed fermentum arcu auctor id. In blandit risus orci, nec pharetra sem convallis pretium. Ut sed suscipit ipsum. Donec vulputate et sapien nec vehicula. Duis bibendum nec nisl at semper. Quisque sodales faucibus justo vel laoreet. Aenean consectetur ex ac urna ornare, eget ultricies lectus iaculis. Curabitur congue congue erat vitae malesuada. Pellentesque varius nunc nibh, ut tincidunt elit iaculis a. Curabitur dictum eget arcu sed faucibus. In vitae turpis ac odio dapibus facilisis. \n\nAliquam et mauris sed neque mattis placerat eu id nisl. Fusce quis odio in libero posuere pharetra vel at nunc. Integer eu pharetra tellus, at aliquam velit. Duis laoreet tortor vel efficitur gravida. Pellentesque a ex quis risus iaculis luctus. Nam at diam ut odio tempor lobortis at a erat. In vitae felis eget augue rhoncus tempor elementum quis nisl. Curabitur malesuada justo sagittis arcu interdum egestas.'
		}
	},
	{
		id: 2,
		name: "The Sun King's Wrath",
		type: 'Expansion',
		color: '#b2b2b2',
		image: "Assets/Images/scenario03_background.png",
		video: "../Assets/Videos/background03.mp4",
		minPlayers: 1,
		maxPlayers: 4,
		time: "3h±",
		facts:  [
			"When the Sun King finally awakened after decades, he silenced his own heart, believing that anything that made him at all mortal made him weak. Afterwards he journeyed to Myriad's Fall where he raised the ancient massive dragon Myrsfünd, the first consort to Berath, as a frost wyrm, then watched his massive undead army prepare for war.",
			'The Altissimo is the highest peak in the northern part of the Monte-Baldo range.',
			'On top of the Altissimo there is a mountain hut, the Rifugio Damiano Chiesa. The easiest way to reach the top is a hike over a dirt road from Strada Provinciale del Monte Baldo.'
		],
		isConfirmed: Observable(false),
		difficultyBackgroundImage: 'Assets/Images/difficulty_background03.jpg',		difficulties: [
			{
				id: 0,
				name: 'Easy',
				difficultyInfo: "Stronger enemies. Play time will also increase."
			},
			{
				id: 1,
				name: 'Medium',
				difficultyInfo: "Stronger and high healthy enemies. Play time will also increase."
			}
		],
		scenPrep: {
			tokens: [
				{
					token_image: 'Assets/Images/token01.png',
					token_size: 60,
					token_amount: 15
				},
				{
					token_image: 'Assets/Images/token02.png',
					token_size: 60,
					token_amount: 20
				},
				{
					token_image: 'Assets/Images/token03.png',
					token_size: 60,
					token_amount: 30
				},
				{
					token_image: 'Assets/Images/token04.png',
					token_size: 110,
					token_amount: 10
				},
				{
					token_image: 'Assets/Images/token04.png',
					token_size: 110,
					token_amount: 7
				}
			],
			considerations:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n\nUt egestas odio nibh, nec ornare sem suscipit vel. In dictum tellus ut dolor placerat eleifend. Nunc bibendum ac lorem at posuere. Ut sagittis augue justo. Sed ultricies venenatis nibh ut euismod. \n\nUt accumsan urna nunc, eget ullamcorper nunc scelerisque blandit. Etiam scelerisque purus vitae congue vestibulum. Aenean sit amet tempor mauris. Quisque ut quam mollis, volutpat ligula non, hendrerit tellus. Cras pretium ut tortor a malesuada. Morbi varius molestie eleifend. Mauris mattis, tortor id suscipit condimentum, lacus justo viverra leo, tempor faucibus quam eros id arcu. Pellentesque eros ligula, malesuada id erat non, semper posuere odio. Cras at faucibus nibh, sit amet dictum elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin elementum nulla sit amet neque hendrerit, eu lobortis dolor tempus. Aenean congue luctus mattis. Pellentesque in aliquam eros. Duis ac sem condimentum, consequat nibh maximus, pretium lorem. Vivamus dignissim ipsum quis odio vulputate iaculis. Praesent ultrices posuere dolor, a lacinia lacus facilisis id. Mauris tristique libero urna, sed fermentum arcu auctor id. In blandit risus orci, nec pharetra sem convallis pretium. Ut sed suscipit ipsum. Donec vulputate et sapien nec vehicula. Duis bibendum nec nisl at semper. Quisque sodales faucibus justo vel laoreet. Aenean consectetur ex ac urna ornare, eget ultricies lectus iaculis. Curabitur congue congue erat vitae malesuada. Pellentesque varius nunc nibh, ut tincidunt elit iaculis a. Curabitur dictum eget arcu sed faucibus. In vitae turpis ac odio dapibus facilisis. \n\nAliquam et mauris sed neque mattis placerat eu id nisl. Fusce quis odio in libero posuere pharetra vel at nunc. Integer eu pharetra tellus, at aliquam velit. Duis laoreet tortor vel efficitur gravida. Pellentesque a ex quis risus iaculis luctus. Nam at diam ut odio tempor lobortis at a erat. In vitae felis eget augue rhoncus tempor elementum quis nisl. Curabitur malesuada justo sagittis arcu interdum egestas.'
		}
	},
	{
		id: 3,
		name: 'Sunleth Heart',
		type: 'Expansion',
		color: '#c96406',
		image: "Assets/Images/scenario04_background.png",
		video: "../Assets/Videos/background04.mp4",
		minPlayers: 1,
		maxPlayers: 2,
		time: "1h±",
		facts:  [
			"People's hearts are pure chaos. Formless and ever-changing—a mystery no one can solve. That is why humans are such contradictions. One moment they're at each other's throats, the next they're forming shaky alliances. May the heat of the sunleth have them in his sight.",
			"Setagaya has the largest population and second largest area (after Ōta) of Tokyo's 23 special wards.",
			'Setagaya is located at the southwestern corner of the 23 special wards and Tama River separates the boundary between Tokyo Metropolis and Kanagawa Prefecture.'
		],
		isConfirmed: Observable(false),
		difficultyBackgroundImage: 'Assets/Images/difficulty_background04.jpg',		difficulties: [
			{
				id: 0,
				name: 'Easy',
				difficultyInfo: "Stronger enemies."
			}
		],
		scenPrep: {
			tokens: [
				{
					token_image: 'Assets/Images/token01.png',
					token_size: 60,
					token_amount: 15
				},
				{
					token_image: 'Assets/Images/token02.png',
					token_size: 60,
					token_amount: 20
				},
				{
					token_image: 'Assets/Images/token03.png',
					token_size: 60,
					token_amount: 30
				},
				{
					token_image: 'Assets/Images/token04.png',
					token_size: 110,
					token_amount: 10
				},
				{
					token_image: 'Assets/Images/token04.png',
					token_size: 110,
					token_amount: 7
				}
			],
			considerations:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n\nUt egestas odio nibh, nec ornare sem suscipit vel. In dictum tellus ut dolor placerat eleifend. Nunc bibendum ac lorem at posuere. Ut sagittis augue justo. Sed ultricies venenatis nibh ut euismod. \n\nUt accumsan urna nunc, eget ullamcorper nunc scelerisque blandit. Etiam scelerisque purus vitae congue vestibulum. Aenean sit amet tempor mauris. Quisque ut quam mollis, volutpat ligula non, hendrerit tellus. Cras pretium ut tortor a malesuada. Morbi varius molestie eleifend. Mauris mattis, tortor id suscipit condimentum, lacus justo viverra leo, tempor faucibus quam eros id arcu. Pellentesque eros ligula, malesuada id erat non, semper posuere odio. Cras at faucibus nibh, sit amet dictum elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin elementum nulla sit amet neque hendrerit, eu lobortis dolor tempus. Aenean congue luctus mattis. Pellentesque in aliquam eros. Duis ac sem condimentum, consequat nibh maximus, pretium lorem. Vivamus dignissim ipsum quis odio vulputate iaculis. Praesent ultrices posuere dolor, a lacinia lacus facilisis id. Mauris tristique libero urna, sed fermentum arcu auctor id. In blandit risus orci, nec pharetra sem convallis pretium. Ut sed suscipit ipsum. Donec vulputate et sapien nec vehicula. Duis bibendum nec nisl at semper. Quisque sodales faucibus justo vel laoreet. Aenean consectetur ex ac urna ornare, eget ultricies lectus iaculis. Curabitur congue congue erat vitae malesuada. Pellentesque varius nunc nibh, ut tincidunt elit iaculis a. Curabitur dictum eget arcu sed faucibus. In vitae turpis ac odio dapibus facilisis. \n\nAliquam et mauris sed neque mattis placerat eu id nisl. Fusce quis odio in libero posuere pharetra vel at nunc. Integer eu pharetra tellus, at aliquam velit. Duis laoreet tortor vel efficitur gravida. Pellentesque a ex quis risus iaculis luctus. Nam at diam ut odio tempor lobortis at a erat. In vitae felis eget augue rhoncus tempor elementum quis nisl. Curabitur malesuada justo sagittis arcu interdum egestas.'
		}
	},
	{
		id: 4,
		name: 'The Servant of the Deep',
		type: 'Expansion',
		color: '#2b509b',
		image: "Assets/Images/scenario05_background.png",
		video: "../Assets/Videos/background05.mp4",
		minPlayers: 2,
		maxPlayers: 5,
		time: "3h±",
		facts: [
			"Similar to the Dark Sorceries, the power of the Deep consists of the sediment of the Dark, called dregs. It also has the capacity to generate life, as if it were stagnant waters, and is drawn towards living things. What it is exactly is uncertain, but it seems to start in Minithyll with the cleric Berath, who experienced visions of the deep sea.",
			'Provo Canyon splits between Mount Timpanogos on the north and Mount Cascade on the south.',
			'Attractions in Provo Canyon include Vivian Park and Bridal Veil Falls.'
		],
		isConfirmed: Observable(false),
		difficultyBackgroundImage: 'Assets/Images/difficulty_background05.jpg',		difficulties: [
			{
				id: 0,
				name: 'Easy',
				difficultyInfo: "Stronger enemies. Play time will also increase."
			},
			{
				id: 1,
				name: 'Medium',
				difficultyInfo: "Stronger and high healthy enemies. Play time will also increase."
			},
			{
				id: 2,
				name: 'Hard',
				difficultyInfo: "Stronger and high healthy enemies. Play time will also increase, as well the quality of random objects."
			},
			{
				id: 3,
				name: 'Extreme',
				difficultyInfo: "Stronger and heavy healthy enemies. Play time will also highly increase, as well the quality of random objects."
			}
		],
		scenPrep: {
			tokens: [
				{
					token_image: 'Assets/Images/token01.png',
					token_size: 60,
					token_amount: 15
				},
				{
					token_image: 'Assets/Images/token02.png',
					token_size: 60,
					token_amount: 20
				},
				{
					token_image: 'Assets/Images/token03.png',
					token_size: 60,
					token_amount: 30
				},
				{
					token_image: 'Assets/Images/token04.png',
					token_size: 110,
					token_amount: 10
				},
				{
					token_image: 'Assets/Images/token04.png',
					token_size: 110,
					token_amount: 7
				}
			],
			considerations:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n\nUt egestas odio nibh, nec ornare sem suscipit vel. In dictum tellus ut dolor placerat eleifend. Nunc bibendum ac lorem at posuere. Ut sagittis augue justo. Sed ultricies venenatis nibh ut euismod. \n\nUt accumsan urna nunc, eget ullamcorper nunc scelerisque blandit. Etiam scelerisque purus vitae congue vestibulum. Aenean sit amet tempor mauris. Quisque ut quam mollis, volutpat ligula non, hendrerit tellus. Cras pretium ut tortor a malesuada. Morbi varius molestie eleifend. Mauris mattis, tortor id suscipit condimentum, lacus justo viverra leo, tempor faucibus quam eros id arcu. Pellentesque eros ligula, malesuada id erat non, semper posuere odio. Cras at faucibus nibh, sit amet dictum elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin elementum nulla sit amet neque hendrerit, eu lobortis dolor tempus. Aenean congue luctus mattis. Pellentesque in aliquam eros. Duis ac sem condimentum, consequat nibh maximus, pretium lorem. Vivamus dignissim ipsum quis odio vulputate iaculis. Praesent ultrices posuere dolor, a lacinia lacus facilisis id. Mauris tristique libero urna, sed fermentum arcu auctor id. In blandit risus orci, nec pharetra sem convallis pretium. Ut sed suscipit ipsum. Donec vulputate et sapien nec vehicula. Duis bibendum nec nisl at semper. Quisque sodales faucibus justo vel laoreet. Aenean consectetur ex ac urna ornare, eget ultricies lectus iaculis. Curabitur congue congue erat vitae malesuada. Pellentesque varius nunc nibh, ut tincidunt elit iaculis a. Curabitur dictum eget arcu sed faucibus. In vitae turpis ac odio dapibus facilisis. \n\nAliquam et mauris sed neque mattis placerat eu id nisl. Fusce quis odio in libero posuere pharetra vel at nunc. Integer eu pharetra tellus, at aliquam velit. Duis laoreet tortor vel efficitur gravida. Pellentesque a ex quis risus iaculis luctus. Nam at diam ut odio tempor lobortis at a erat. In vitae felis eget augue rhoncus tempor elementum quis nisl. Curabitur malesuada justo sagittis arcu interdum egestas.'
		}
	}
);
 
