var Observable = require('FuseJS/Observable');
var FileSystem = require("FuseJS/FileSystem");

var activeScenario = Observable(0);

var descrText = Observable('Holaaaa');

var json = [];

function Item(image, category, name, text, players, elapsedTime){
	this.category = category;
	this.name = name;
	this.text = text;
	this.image = image;
	this.players = players;
	this.elapsedTime = elapsedTime;
}

var items = Observable();

// Process data file
var dataPath = FileSystem.dataDirectory + "/" + "scenarios.json";

FileSystem.readTextFromFile(dataPath)
.then(function(result) {
	json = JSON.parse(result);
  for (var i=0; i<JSON.parse(result).length; i++) {
		items.add(new Item(json[i].image, json[i].category, json[i].name, json[i].text, json[i].players, json[i].elapsedTime));
	}
});

for (var i=0; i<json.length; i++) {
	items.add(new Item(json[i].image, json[i].category, json[i].name, json[i].text, json[i].players, json[i].elapsedTime));
}

function goToScenDescr() {
	console.log('Choosen Scenario: ', activeScenario.value);
}




module.exports = {
	items: items,
	activeScenario: activeScenario,
	goToScenDescr: goToScenDescr,
	descrText: descrText
};