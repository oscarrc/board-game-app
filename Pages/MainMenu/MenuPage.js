var Observable = require('FuseJS/Observable');



function goToNewGame() {
    router.push("NewGamePage");
}

function continueProject() {
	// router.push("????Page");
}

function goToOptions() {
	router.push("OptionsPage");
}

var allLoaded = Observable(false);

function loadInfo() {
    setTimeout(function() {
        allLoaded.value = true;
    }, 3200);
}

module.exports = {
    goToNewGame: goToNewGame,
    continueProject: continueProject,
    goToOptions: goToOptions,
    loadInfo: loadInfo,
    allLoaded: allLoaded
};