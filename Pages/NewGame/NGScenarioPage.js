var Observable = require('FuseJS/Observable');
var FileSystem = require("FuseJS/FileSystem");

var backHeight = Observable();


// Reads and map the observable inside scenarios.json, containing the basic data
var scenarios = require("scenarios");

var current = Observable();
var inDetailsMode = current.map(function(x){
	return !!x;
});

function refreshData() {
	scenarios.forEach(function(item) {
		item.isConfirmed.value = false;
	});
}




var json;
var configsPath = FileSystem.dataDirectory + "/" + "config.json";

// Called when an Item is selected. Writes the selected scenario variable
// inside config.json. At the same time, refreshInfo is called in the
// MainView to read the selected_scenario variable and change the background video.
function refreshSelectItem(e){
	FileSystem.readTextFromFile(configsPath)
    	.then(function(result) {
			json = JSON.parse(result);
			json.selected_scenario = e.data.id;
			FileSystem.writeTextToFile(configsPath, JSON.stringify(json));
			isAnySelected = true;
    	});
}


// When the Confirm button of an item is pressed, the confirmed_scenario
// variable in the config.json is set. Then, the scenarios Observable
// is refreshed with the new info.
function selectItem(e) {
	json.confirmed_scenario = e.data.id;
	FileSystem.writeTextToFile(configsPath, JSON.stringify(json));

	// var selectedItem = e.data;
	// selectedItem.isConfirmed = true;
	// scenarios.set(e.data.id, selectedItem);

	scenarios.forEach(function(item) {
    	if (item.id != e.data.id) {
			item.isConfirmed.value = false;
    	}
	});

	scenarios.getAt(e.data.id).isConfirmed.value = true;
}


module.exports = {
	backHeight: backHeight,
	scenarios: scenarios.map(function(item, index){
		item.alignment = index % 2 === 0 ? "Left" : "Right";
		return item;
	}),
	current: current,
	refreshData: refreshData,
	inDetailsMode: inDetailsMode,
	refreshSelectItem: refreshSelectItem,
	selectItem: selectItem
};
