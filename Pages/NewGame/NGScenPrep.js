var Observable = require('FuseJS/Observable');
var FileSystem = require("FuseJS/FileSystem");
var scenarios = require("scenarios");

function goToMenu() {
    router.goto("MenuPage");
}


function Token(token_image, token_size, token_amount){
    this.token_image = token_image;
    this.token_size = token_size;
    this.token_amount = token_amount;
}


var tokens = Observable();
var considerations = Observable();

// Reads the config inside config.json
function loadConfig() {
    var configsPath = FileSystem.dataDirectory + "/" + "config.json";
    FileSystem.readTextFromFile(configsPath)
        .then(function(result) {
            var json = JSON.parse(result);
            var scenPrep = scenarios.getAt(json.confirmed_scenario).scenPrep.tokens;

            var newTokenList = [];
            for(var i = 0; i < scenPrep.length; i++) {
                newTokenList.push(new Token(scenPrep[i].token_image, scenPrep[i].token_size, scenPrep[i].token_amount));
            }
            tokens.replaceAll(newTokenList);

            considerations.value = scenarios.getAt(json.confirmed_scenario).scenPrep.considerations;
        });
}

function goToCharPrep() {
    router.goto("NGCharPrep");
}


module.exports = {
    goToMenu: goToMenu,
    loadConfig: loadConfig,
    tokens: tokens,
    considerations: considerations,
    goToCharPrep: goToCharPrep
};
