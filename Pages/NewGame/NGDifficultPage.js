var Observable = require('FuseJS/Observable');
var FileSystem = require("FuseJS/FileSystem");



var json;
var configsPath = FileSystem.dataDirectory + "/" + "config.json";

// Reads and map the observable inside scenarios.json, containing the basic data
var scenarios = require("scenarios");
var difficulties = Observable();
var generalBackgroundImage = Observable();

var activePageIndex = Observable();
var leftArrowVisible = Observable('Collapsed');
var rightArrowVisible = Observable('Collapsed');


function Difficulty(id, name, difficultyInfo){
    this.id = id;
    this.name = Observable(name.toUpperCase());
    this.renderImage = Observable();
    this.color = Observable();
    this.opacity = Observable();

    switch(id) {
        case 0:
            this.renderImage.value = 'Assets/Images/difficult_easy_render.png';
            this.color.value = null;
            this.opacity.value = 0.5;
            break;
        case 1:
            this.renderImage.value = 'Assets/Images/difficult_medium_render.png';
            this.color.value = '#274493';
            this.opacity.value = 0.5;
            break;
        case 2:
            this.renderImage.value = 'Assets/Images/difficult_hard_render.png';
            this.color.value = '#933d27';
            this.opacity.value = 0.6;
            break;
        case 3:
            this.renderImage.value = 'Assets/Images/difficult_extreme_render.png';
            this.color.value = null;
            this.opacity.value = 0.9;
            break;
      }

    this.difficultyInfo = Observable(difficultyInfo);
    this.isSelected = Observable(false);
}


// Load info when page becomes Active
function loadDifficulties() {
    FileSystem.readTextFromFile(configsPath)
    .then(function(result) {
        json = JSON.parse(result);
        
        generalBackgroundImage.value = scenarios.getAt(json.selected_scenario).difficultyBackgroundImage;

        var itemDifficulties = scenarios.getAt(json.selected_scenario).difficulties;
        var newDifficultyList = [];

        for(var i = 0; i < itemDifficulties.length; i++) {
            newDifficultyList.push(new Difficulty(itemDifficulties[i].id, itemDifficulties[i].name, itemDifficulties[i].difficultyInfo));
        }
        difficulties.replaceAll(newDifficultyList);
    });
}


// When a difficulty is selected, that button becomes unclickable, and the value is written in the config.js
function selectDifficulty(x) {
    difficulties.forEach(function(item) {
    	if (item.id === x.data.id) {
			item.isSelected.value = true;
        } else {
            item.isSelected.value = false;
        }
    });
    json.selectedDifficulty = x.data.id;
    FileSystem.writeTextToFile(configsPath, JSON.stringify(json));
}


// Triggered every time the activePageIndex observable change
activePageIndex.onValueChanged(module, function() {
    if (json) {
        if (activePageIndex.value === 0) {
            leftArrowVisible.value = 'Collapsed';
        } else {
            leftArrowVisible.value = 'Visible';
        }

        if (activePageIndex.value == (scenarios.getAt(json.selected_scenario).difficulties.length - 1)) {
            rightArrowVisible.value = 'Collapsed';
        } else {
            rightArrowVisible.value = 'Visible';
        }
}
});




function backDifficulty() {
    activePageIndex.value --;
}

function nextDifficulty() {
    activePageIndex.value ++;
}


module.exports = {
    loadDifficulties: loadDifficulties,

    activePageIndex: activePageIndex,
    leftArrowVisible: leftArrowVisible,
    rightArrowVisible: rightArrowVisible,

    generalBackgroundImage: generalBackgroundImage,
    difficulties: difficulties.map(function(item, index){
		item.id  === json.selectedDifficulty ? item.isSelected.value = true : item.isSelected.value = false;
		return item;
	}),
    selectDifficulty: selectDifficulty,
    backDifficulty: backDifficulty,
    nextDifficulty: nextDifficulty
};